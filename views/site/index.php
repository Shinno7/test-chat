<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">MEGO CHAT!</h1>
    </div>

    <div class="body-content">
        <div id="counter">
            <?php foreach ($messages as $message): ?>
                <p>
                    <?=$message->name?>:
                    <?= $message->content ?>
                </p>
            <?php endforeach; ?>
        </div>
        <?php $form = ActiveForm::begin(['id' => 'send-message']); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => 255, 'required'=>true]) ?>
        <?= $form->field($model, 'content')->textarea(['maxlength' => 7000, 'required'=>true]) ?>
        <div class="form-group">
            <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
    <script type="text/javascript">

        const container = document.getElementById('counter')
        const centrifuge = new Centrifuge("ws://localhost:8000/connection/websocket");
        centrifuge.setToken("<?=$token?>");

        centrifuge.on('connect', function(ctx) {
            console.log("connected", ctx);
        });

        centrifuge.on('disconnect', function(ctx) {
            console.log("disconnected", ctx);
        });

        centrifuge.subscribe("chat", function(ctx) {
            console.log(ctx.data);
           // container.innerHTML = ctx.data.message;
            $("#counter").append("<p>"+$('#messages-name').val()+":"+ctx.data.message+"</p>");
            document.title = ctx.data.value;
        });

        centrifuge.connect();

    </script>
</div>
