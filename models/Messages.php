<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Query;

class Messages extends ActiveRecord
{

    public $datingName;
    public $typeName;

    public $areaTemp;

    public static function tableName()
    {
        return '{{%messages}}';
    }

    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'length' => [1, 255]],
            [['content'], 'string', 'length' => [1, 7000]],
            [['name', 'content'],
                'onlyText'],
        ];
    }

    public function onlyText($attribute)
    {
        if ($this->$attribute != strip_tags($this->$attribute))
            $this->addError($attribute, 'Строка содержит посторонние символы.');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'content' => 'Message',
        );
    }

}
