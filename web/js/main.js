/**
 * Created by User on 24.09.2021.
 */
$(document).ready(function(){
    $('#send-message').on('beforeSubmit', function(e) {
        var form = $(this);
        var formData = form.serialize();
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {
                alert('Test');
            },
            error: function () {
                alert("Something went wrong");
            }
        });
    }).on('submit', function(e){
        e.preventDefault();
    });
});